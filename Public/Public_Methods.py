import requests
import json

class methods():

    todos=''

    # Вызов get методов в Basic auth и обработка статуса ответа
    def Get_method_Basic_auth(self, link, id, auth):
        url = "{0}/{1}".format(link,id)

        payload = {}
        headers = {
            'Authorization': auth,
            'Cookie': '_octo=GH1.1.1389307418.1599036547; logged_in=no'
        }

        response = requests.request("GET", url, headers=headers, data=payload, timeout=3)
        # проверка формата ответа сервиса
        if 'application/json' in response.headers.get('Content-Type'):
            # проверка статуса
            if response.status_code == 200:
                self.todos = response.text
                return True
            elif response.status_code == 401:
                return 'Не корректная авторизация'
            elif response.status_code == 404:
                return 'Данные не найдены'
            else:
                return 'Сервис не доступен'
        else:
            return 'Формат ответа не соответствует ожидаемому (ожидается Json)'

    def check_data(self, teg, text):
        if teg in self.todos:
            resp = json.loads(self.todos)
            x=resp[teg]
            if x[0] == text:
                return True
            else:
                return 'Данные параметра ' + teg + ' не совпадают с ожидаемыми'
        else:
            return 'Параметр ' + teg + ' не найден в ответе'
