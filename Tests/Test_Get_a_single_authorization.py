import unittest
from Public.Public_Methods import *


class test_get_a_single_auth(methods, unittest.TestCase):

    #рабочий только первый кейс, остальные создал для проверки всех типов ошибок

    def test_01(self):
        #'Отправка запроса'
        self.assertEqual(self.Get_method_Basic_auth(link='https://api.github.com/authorizations', id='478643910', auth='Basic bnZ2MTk4NToxMjM0d3dAQEA='), True)
        #'Проверка данных на наличие параметра scopes и проверка его значения'
        self.assertEqual(self.check_data(teg='scopes', text='public_repo'), True)

    def test_02(self):
        #'Отправка запроса'
        self.assertEqual(self.Get_method_Basic_auth(link='https://api.github.com/authorizations', id='478643910', auth='Error'), True)
        #'Проверка данных на наличие параметра scopes и проверка его значения'
        self.assertEqual(self.check_data(teg='scopes', text='public_repo'), True)

    def test_03(self):
        #'Отправка запроса'
        self.assertEqual(self.Get_method_Basic_auth(link='https://api.github.com/authorizations', id='Error', auth='Basic bnZ2MTk4NToxMjM0d3dAQEA='), False)
        #'Проверка данных на наличие параметра scopes и проверка его значения'
        self.assertEqual(self.check_data(teg='scopes', text='public_repo'), True)

    def test_04(self):
        #'Отправка запроса'
        self.assertEqual(self.Get_method_Basic_auth(link='https://api.github.com/authorizations', id='478643910', auth='Basic bnZ2MTk4NToxMjM0d3dAQEA='), True)
        #'Проверка данных на наличие параметра scopes и проверка его значения'
        self.assertEqual(self.check_data(teg='scopes', text='Error'), True)

    def test_05(self):
        #'Отправка запроса'
        self.assertEqual(self.Get_method_Basic_auth(link='https://api.github.com/authorizations', id='478643910', auth='Basic bnZ2MTk4NToxMjM0d3dAQEA='), True)
        #'Проверка данных на наличие параметра scopes и проверка его значения'
        self.assertEqual(self.check_data(teg='Error', text='public_repo'), True)

if __name__ == '__main__':
    unittest.main()